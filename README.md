Full-Teaching Orchestration in Jenkins, part of the EMS leverage for the Elastest components and verticals

This repo contains a "main" Jenkins job that orchestrates the calls to the tests (the other jenkins jobs ft[1..3])

Configuration of the Orchestration Library in Jenkins: (anything not explained below is left with the default values)
Jenkins main page -> Manage Jenkins -> Configure System
Global Pipeline Libraries{
Name: OrchestrationLib
Default version: master
Retrieval Method: Modern SCM
GitHub {
Owner: elastest
Repository: elastest-orchestration-engine
}
}
SAVE
