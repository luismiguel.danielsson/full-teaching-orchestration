from websocket import create_connection
import requests
import logging
import sys
import json

def getEMSstate(ems, outputPort, eventsPort, triggerEvent):
    #subscription to EMS
    url = "ws://" + ems + ":" + outputPort #3232
    logging.info("Subscribing to EMS output with url: %s", url)
    ws = create_connection(url)
    triggerEvent = json.loads(triggerEvent)
    #POSTING get
    url = "http://" + ems + ":" + eventsPort #8181
    logging.info("POSTING get to EMS eventPort with url: %s, with content %s", url, json.dumps(triggerEvent))
    response = requests.post(url, json=triggerEvent)
    logging.info("Response from EMS POST: %s, %s", response, response.content)

    #Retrieving state from EMS
    state = ws.recv()
    state = json.loads(state)
    logging.info("State from EMS: %s", state)
    return state

#MAIN
if (len(sys.argv) < 5):
    print ("Need emsIp, emsOutputPort, emsEventsPort, triggerEvent")
    exit(1)

logging.basicConfig(format='%(levelname)s - %(asctime)s - %(message)s', level=logging.INFO, filename='./logs/getEMSstate_log.txt', filemode='w')
encoding = "utf-8"
ems = sys.argv[1]
outputPort = sys.argv[2]
eventsPort = sys.argv[3]
triggerEvent = sys.argv[4]
result = getEMSstate(ems, outputPort, eventsPort, triggerEvent)
print (json.dumps(result))
