import subprocess
import sys
import shlex

if len(sys.argv) < 3:
    print "Need #of copies and command to execute with its parameters"

command = sys.argv[2]
prog = shlex.split(command)
n = int(sys.argv[1])
proc=[]
for i in range(n):
    proc.append(subprocess.Popen(prog))
    #log.write(proc.stdout.read())
    print str(proc[i].pid)

"""
python spawner.py 20 'sleep 200'
"""
