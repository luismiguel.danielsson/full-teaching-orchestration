import socket
import sys
import logging
import json

def send(sutIp, sutPort, msg):
    sutAddr = (sutIp, sutPort)
    # Create a TCP/IP socket
    sock = socket.create_connection(sutAddr)
    logging.info('created connection to Sut: %s', sutAddr)
    # Bind the socket to the address given on the command line
    try:
        json.loads(msg)
        msg = msg + "\n"
        logging.info('sending msg: "%s"', msg)
        sock.sendall(msg.encode(encoding))
        last = ''
        response = ""
        end = "\n"
        while last != end:
            logging.debug("expecting more data")
            response += sock.recv(recv_size).decode(encoding)
            logging.debug('RESPONSE raw: "%s"', response)
            if response:
                last = response[len(response)-1]
                logging.debug('last char: "%s"', last)
            else:
                last = end
        logging.info('RESPONSE: {"request": "%s", "response":"%s"}', msg, json.dumps(json.loads(response)))
        return json.dumps(json.loads(response))
    except Exception as e:
        logging.exception(e)
    finally:
        #sock.shutdown(socket.SHUT_RDWR)
        logging.info('closing socket to Sut "%s"', sutAddr)
        sock.close()

#MAIN
sutIp = sys.argv[1]
sutPort = int(sys.argv[2])
msg = sys.argv[3]
"""emsIp = sys.argv[4]
emsOutputPort = sys.argv[5]
emsEventsPort = sys.argv[6]
emsTriggerEvent = sys.argv[7]"""
logging.basicConfig(format='%(levelname)s - %(asctime)s - %(message)s', level=logging.INFO, filename='./client_log.txt', filemode='w')
encoding = "utf-8"
recv_size = 20
print send(sutIp, sutPort, msg)

"""
python -m py_compile login.py
chmod +x ./login.pyc
python ./login.pyc
"""
#./rebuild_push.sh sut
#docker run --name orch_sut -p 10000:10000 -d --rm luismigueldanielsson/elastest-luismi:ems_orchestration_sut

#python client.py localhost 10000 '{"operation":"login", "args":["a"]}'
